import os


######################################################################################################
# Allgemeines

# Methoden die mit __ beginnen sind private Methoden einer Klasse, sie können nur innerhalb der Klasse
# verwendet werden

# Zugriffe die mit self. beginnen verweisen auf die eigene Instanz eines Objektes

# Das "None" Objekt ist in Python ein Stellvertreter für "Nichts", None besitzt keine Eigenschaften und
# keine Methoden

# Classes in Objektorientierten Programmiersprachen sind grob vergleichbar mit Structs in C
# Structs in C sind eine Möglichkeit zusammenhänge Informationen in einer einzelnen Variablen zu
# Bündeln.
# Beispielsweise könnte man in C ein Auto als Struct definieren, indem man ihn aus den folgenden
# Primitiven Datentypen kombiniert:
#
# struct Auto {
# char[] name,
# int baujahr,
# int anz_türen,
# char[] modell,
# char[] hersteller }
#
# Classes bieten das gleiche, in Python werden solche Felder in der __init__ Methode einer Klasse
# definiert.
# Zusätzlich können Classes aber auch Methoden haben, welche dann beim aufruf von der erzeugten Instanz
# ausgeführt werden
#######################################################################################################


# Die GPIO Class dient als Factory zum erzeugen von Pin Objekten.
# Sie hat hält ein Array von Tupeln für jeden Pin
class GPIO:

    # Im Raspberry Dateisystem werden dient der Pfad als "root" Verzeichnis für die Pins
    # Wird ein Pin N exportiert, so wird ein Directory mit dem Namen "gpioN" angelegt,
    # in dem Informationen über den Pin vom System abgelegt werden
    path = "/sys/class/gpio"

    # __init__ ist die erste Methode, die ein Objekt grundsätzlich immer ausführt, wenn eine Instanz
    # davon erzeugt wird.
    def __init__(self):
        # pins ist der Array von Tupeln für jeden Pin
        self.pins = []
        # Initialisiert den Pins Array
        self.__set_pins()
        return

    # __set_pins initialisiert den Pin Array.
    # Für jeden Pin wird ein Element in den Array eingefügt, die Elemente sind hier ein Tupel von 4 Informationen
    # BCM, wPi, Name, Physische Position des Pins
    # Beipiel: [0, 30, "SDA0", 27]
    # 0 ist die BCM Nummer des SDA0 Pins
    # 30 ist die wPi Nummer des SDA0 Pins
    # SDA0 befindet sich an der physikalischen Poition 27
    def __set_pins(self):
        self.pins.extend([
            [0, 30, "SDA0", 27],
            [1, 31, "SCL0", 28],
            [2, 8, "SDA1", 3],
            [3, 9, "SCL1", 5],
            [4, 7, "GPIO7", 7],
            [5, 21, "GPIO21", 29],
            [6, 22, "GPIO22", 31],
            [7, 11, "CE1", 26],
            [8, 10, "CE0", 24],
            [9, 13, "MISO", 21],
            [10, 12, "MOSI", 19],
            [11, 14, "SCLK", 23],
            [12, 26, "GPIO26", 32],
            [13, 23, "GPIO23", 33],
            [14, 15, "TxD", 8],
            [15, 16, "RxD", 10],
            [16, 27, "GPIO27", 36],
            [17, 0, "GPIO0", 11],
            [18, 1, "GPIO1", 12],
            [19, 24, "GPIO24", 35],
            [20, 28, "GPIO28", 38],
            [21, 29, "GPIO29", 40],
            [22, 3, "GPIO3", 15],
            [23, 4, "GPIO4", 16],
            [24, 5, "GPIO5", 18],
            [25, 6, "GPIO6", 22],
            [26, 25, "GPIO25", 37],
            [27, 2, "GPIO2", 13]])

    # Erzeugt ein Pin Objekt für den Pin mit der BCM-Nummer bcm
    # Wird die Methode für einen BCM aufgerufen, dessen Pin Objekt bereits existiert, so wird das existierende
    # Objekt zurückgegeben
    def get_pin_by_bcm(self, bcm):
        # Es wird getestet, ob die bcm Nummer größer ist, als die länge es Arrays, falls ja würde der Index Zugriff
        # über den Rand des Arrays hinaus gehen
        if bcm > len(self.pins):
            # Falls die bcm Nummer außerhalb des Arrays liegt, wird None zurück gegeben
            return None
        # Überprüft die Länge des Tupels an der Stelle bcm in pins
        if len(self.pins[bcm]) < 5:
            # Falls die Länge kleiner ist als 4, wurde noch kein Pin Objekt für diesen bcm erzeugt
            # Es wird ein Pin erzeugt und in den Array eingefügt
            self.pins[bcm].append(Pin(self.pins[bcm][0], self.pins[bcm][1], self.pins[bcm][2], self.pins[bcm][3]))
        # Es wird das Pin Objekt zurückgegeben
        return self.pins[bcm][4]

    # Erzeugt ein Pin Objekt für den Pin mit der wPi-Nummer wpi
    # Wird die Methode für einen wpi aufgerufen, dessen Pin Objekt bereits existiert, so wird das existierende
    # Objekt zurückgegeben
    def get_pin_by_wpi(self, wpi):
        # For-Schleife Iteriert über alle Zahlen von 0 bis "länge von pins"
        for i in range(0, len(self.pins)):
            # Falls der Pin im Array an der Position i den gesuchten wpi hat, wird get_pin_by_bcm mit der dazugehörigen
            # bcm aufgerufen
            if self.pins[i][1] == wpi:
                # Gibt das ergebnis von get_pin_by_bcm zurück
                return self.get_pin_by_bcm(self.pins[i][0])
        # Falls kein Objekt mit dem angegebenen wpi gefunden wird, gibt die Methode None zurück
        return None

    # Erzeugt ein Pin Objekt für den Pin mit dem Namen name
    # Wird die Methode für einen Namen aufgerufen, dessen Pin Objekt bereits existiert, so wird das existierende
    # Objekt zurückgegeben
    def get_pin_by_name(self, name):
        # For-Schleife Iteriert über alle Zahlen von 0 bis "länge von pins"
        for i in range(0, len(self.pins)):
            # Falls der Pin im Array an der Position i den gesuchten Namen hat, wird get_pin_by_bcm mit dem
            # dazugehörigen bcm aufgerufen
            if self.pins[i][2] == name:
                # Gibt das ergebnis von get_pin_by_bcm zurück
                return self.get_pin_by_bcm(self.pins[i][0])
        # Falls kein Objekt mit dem angegebenen Namen gefunden wird, gibt die Methode None zurück
        return None

    # Erzeug ein Pin Objekt für den Pin an der physikalischen Position physical
    # Wird die Methode für einen physischen Pin aufgerufen, dessen Objekt bereits existiert, so wird das existierende
    # Objekt zurückgegeben
    def get_pin_by_physical(self, physical):
        # For-Schleife Iteriert über alle Zahlen von 0 bis "länge von pins"
        for i in range(0, len(self.pins)):
            # Falls der Pin im Array an der Position i die gesuchte physikalische Position hat, wird get_pin_by_bcm
            # mit dem dazugehörigen bcm aufgerufen
            if self.pins[i][3] == physical:
                # Gibt das ergebnis von get_pin_by_bcm zurück
                return self.get_pin_by_bcm(self.pins[i][0])
        # Falls kein Objekt mit der angegebenen physikalischen Position existiert, gibt die Methode None zurück
        return None


class Pin:

    value_ext = "/value"
    direction_ext = "/direction"
    edge_ext = "/edge"
    active_low_ext = "/active_low"

    def __init__(self, bcm, wpi, name, physical):
        self.bcm = bcm
        self.wpi = wpi
        self.name = name
        self.physical = physical
        self.path = GPIO.path + "/gpio" + str(self.bcm)
        self.exported = False
        self.__set_exported()

    def __set_exported(self):
        if os.path.isdir(GPIO.path + "/gpio" + str(self.bcm)):
            self.exported = True
        else:
            self.exported = False

    def export(self):
        if self.exported:
            return
        os.system("gpio export " + str(self.bcm) + " in")
        self.__set_exported()

    def unexport(self):
        os.system("gpio unexport " + str(self.bcm))
        self.__set_exported()

    def set_mode(self, mode):
        if not (mode == "in" or mode == "out"):
            return
        os.system("gpio -g mode " + str(self.bcm) + " " + mode)

    def get_mode(self):
        if not self.exported:
            return
        file = open(self.path + Pin.direction_ext)
        return file.read()

    def set_value(self, value):
        if not (value == 0 or value == 1):
            return
        if not self.exported:
            return
        os.system("gpio -g write " + str(self.bcm) + " " + str(value))

    def get_value(self):
        if not self.exported:
            return
        file = open(self.path + Pin.value_ext)
        return int(file.read())

    def set_edge(self, edge):
        if not (edge == "none" or edge == "rising" or edge == "falling" or edge == "both"):
            return
        if not self.exported:
            return
        os.system("gpio edge " + str(self.bcm) + " " + edge)

    def get_edge(self):
        if not self.exported:
            return
        file = open(self.path + Pin.edge_ext)
        return file.read()

    def set_active_low(self, active_low):
        if not (active_low == 0 or active_low == 1):
            return
        if not self.exported:
            return
        os.system("echo " + str(active_low) + " > " + self.path + Pin.active_low_ext)

    def get_active_low(self):
        if not self.exported:
            return
        file = open(self.path + Pin.active_low_ext)
        return int(file.read())

    def toggle(self):
        if not self.exported:
            return
        os.system("gpio toggle " + str(self.wpi))
